<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoriaController extends Controller
{

    public function index()
    {
        $categorias = Categoria::paginate(15);
        // dd($categorias);
        return view('admin.categorias.index', [
            'categorias' => $categoria
        ]);
    }

    public function create()
    {
        return view('admin.categorias.cadastrar',[
            'categoria' => new Categoria()
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'titulo' => 'required',
            'cor' => 'required',
            'icone' => 'required',
        ]);

        $categoria = new Categoria();
        $categoria->titulo = $request->titulo;
        $categoria->cor = $request->cor;

            if($request->hasFile('icone')){

                //Upload com nome do random do arquivo..
                // $iconePath = $request->file('icone')->store('public/icones');
                // $categoria->icone = Storage::url($iconePath);

                //Upload mantendo o mesmo nome do arquivo
                $icone = $request->file('icone');
                $iconeNomeOriginal = $icone->getClientOriginalName();
                $iconePath = $icone->storeAS('public/icones', $iconeNomeOriginal);
                $categoria->icone = Storage::url($iconePath);
            }
            $categoria->save();
            return redirect()->route('admin.categorias.index')
                ->with('sucesso', 'Categoria Excluida com Sucesso!');
        }

    public function edit($id)
    {
        $categoria = Categoria::findOrFail($id);

        return view('admin.categorias.editar',
             ['categoria' => $categoria]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'titulo' => 'required',
            'cor' => 'required',

        ]);

        $categoria =  Categoria::findOrFail($id);
        $categoria->titulo = $request->titulo;
        $categoria->cor = $request->cor;

            if($request->hasFile('icone')){
                Storage::delete('public/icones/'.basename($categoria->icone));

                $icone = $request->file('icone');
                $iconeNomeOriginal = $icone->getClientOriginalName();
                $iconePath = $icone->storeAS('public/icones', $iconeNomeOriginal);
                $categoria->icone = Storage::url($iconePath);
            }
            $categoria->save();
            return redirect()->route('admin.categorias.index')
                ->with('sucesso', 'Categoria Atualizada com Sucesso!!');
    }

    public function destroy($id)
    {
        $categoria = Categoria::findOrFail($id);

        Storage::delete('public/icones/'.basename($categoria->icone));
        if ($categoria->delete()) {


            return redirect()->route('admin.categorias.index')->with('sucesso', 'Categoria Excluida com Sucesso!');
        } else {

            return redirect()->route('admin.categorias.index')->with('erro', 'Erro a Excluir a Categoria');
        }

        // $categoria->delete();
        // return redirect()->route('admin.categorias.index')->with('Sucesso','Categoria Excluida com Sucesso!');
    }
}
