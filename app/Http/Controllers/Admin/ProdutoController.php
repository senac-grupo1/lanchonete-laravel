<?php

namespace App\Http\Controllers\Admin;

use App\Models\Produto;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProdutoController extends Controller
{

    public function index()
    {
        $produtos = Produto::paginate(15);
        // dd($categorias);
        return view('admin.produtos.index', [
            'produtos' => $produtos
        ]);
    }

    public function create()
    {

        return view('admin.produtos.cadastrar', [
            'categorias' => Categoria::all(),
            'produtos' => new Produto()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'valor' => 'required',
            'foto' => 'required',
            'categoria_id' => 'required',
        ]);

        $produtos = new Produto();
        $produtos->titulo = $request->titulo;
        $produtos->descricao = $request->descricao;
        $produtos->valor = $request->valor;
        $produtos->categoria_id = $request->categoria_id;

        if($request->hasFile('foto')){

            $fotoPath = $request->file('foto')->store('public/produtos');
            $produtos->foto = Storage::url($fotoPath);
        }
        $produtos->save();
        return redirect()->route('admin.produtos.index')
            ->with('sucesso', 'Produto cadastrada com Sucesso!');
    }

    public function edit($id)
    {
        $produtos = Produto::findOrFail($id);

        return view('admin.produtos.editar', [
            'produtos' => $produtos,
            'categorias' => Categoria::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'valor' => 'required',
            'categoria_id' => 'required',
        ]);

        $produtos = Produto::findOrFail($id);
        $produtos->titulo = $request->titulo;
        $produtos->descricao = $request->descricao;
        $produtos->valor = $request->valor;
        $produtos->categoria_id = $request->categoria_id;
        $produtos->foto = $request->foto;

        if($request->hasFile('foto')){
            Storage::delete('public/produtos/'.basename($produtos->foto));

            $fotoPath = $request->file('foto')->store('public/produtos');
            $produtos->foto = Storage::url($fotoPath);
        }

        $produtos->save();
        return redirect()->route('admin.produtos.index')
            ->with('sucesso', 'Produto Atualizado com Sucesso!');
    }

    public function destroy($id)
    {
        $produtos = Produto::findOrFail($id);

        Storage::delete('public/produtos/'.basename($produtos->icone));
        if ($produtos->delete()) {


            return redirect()->route('admin.produtos.index')->with('sucesso', 'Produto Excluido com Sucesso!');
        } else {

            return redirect()->route('admin.produtos.index')->with('erro', 'Erro a Excluir o Produto');
        }
    }
}
