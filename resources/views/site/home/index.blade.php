@extends('layouts.site')

@section('conteudo')

<div class="listaCategorias">
            <ul>

                <li>
                    <div class="itemCategoria">
                        <img src="img/lanche.svg" alt="Lanche" height="30px">
                        <h2>Lanche</h2>
                    </div>
                </li>

                <li>
                    <div class="itemCategoria">
                        <img src="img/fritas.svg" alt="Porção" height="30px">
                        <h2>Porção</h2>
                    </div>
                </li>

                <li>
                    <div class="itemCategoria">
                        <img src="img/bebida.svg" alt="Bebida" height="30px">
                        <h2>Bebidas</h2>
                    </div>
                </li>

                <li>
                    <div class="itemCategoria">
                        <img src="img/suco.svg" alt="Suco" height="30px">
                        <h2>Suco</h2>
                    </div>
                </li>
                <li>
                    <div class="itemCategoria">
                        <img src="img/pizza.svg" alt="Pizza" height="30px">
                        <h2>Pizza</h2>
                    </div>
                </li>

            </ul>
        </div>

        <div class="listaProdutos">
            <h1>Vamos faça o seu pedido agora!!</h1>
            <p>estes são os melhores proutos que temos!!</p>
            <div class="itemProdutos">

            <div class="hamburguer">
                <img src="img/x-salada-duplo.jpg" alt="X Salada Duplo"  class="img-fluid">
            </div>
                <h2>X-Salada Duplo</h2>
                <small class="descricao">Dois hambúrguers artesanais,
                    maionese, catchup,
                    mostarda, milho, ervilha,
                    tomate, cebola, queijo
                    mussarela, alface e ovo frito.</small>

                    <div class="boxPreco">
                <span class="preco">R$ 32,99</span>
                <a href="#">
                    <i class="icon-adicionar btnAdicionar"></i>
                </a>
            </div>
            </div>

            <div class="itemProdutos">
                <div class="hamburguer">
                    <img src="img/x-salada-duplo.jpg" alt="X Salada Duplo"  class="img-fluid" >
                </div>
                <h2>X-Salada Duplo</h2>
                <small class="descricao">Dois hambúrguers artesanais,
                    maionese, catchup,
                    mostarda, milho, ervilha,
                    tomate, cebola, queijo
                    mussarela, alface e ovo frito.</small>

                    <div class="boxPreco">
                <span class="preco">R$ 32,99</span>
                <a href="#">
                    <i class="icon-adicionar btnAdicionar"></i>
                </a>
            </div>
            </div>

            <div class="itemProdutos">
                <div class="hamburguer">
                    <img src="img/x-salada-duplo.jpg" alt="X Salada Duplo"  class="img-fluid" >
                </div>
                <h2>X-Salada Duplo</h2>
                <small class="descricao">Dois hambúrguers artesanais,
                    maionese, catchup,
                    mostarda, milho, ervilha,
                    tomate, cebola, queijo
                    mussarela, alface e ovo frito.</small>

                    <div class="boxPreco">
                <span class="preco">R$ 32,99</span>
                <a href="#">
                    <i class="icon-adicionar btnAdicionar"></i>
                </a>
                    </div>
            </div>

            <div class="itemProdutos">
                <div class="hamburguer">
                    <img src="img/x-salada-duplo.jpg" alt="X Salada Duplo"  class="img-fluid" >
                </div>
                <h2>X-Salada Duplo</h2>
                <small class="descricao">Dois hambúrguers artesanais,
                    maionese, catchup,
                    mostarda, milho, ervilha,
                    tomate, cebola, queijo
                    mussarela, alface e ovo frito.</small>

                    <div class="boxPreco">
                <span class="preco">R$ 32,99</span>
                <a href="#">
                    <i class="icon-adicionar btnAdicionar"></i>
                </a>
            </div>
        </div>

        </div>
@endsection
