@extends('layout.site')

@section('conteudo')

        <div class="listaProdutos">

            <div class="hamburguer">
                <img src="/img/x-salada-duplo.jpg" alt="X Salada Duplo"  class="img-fluid">
            </div>
                <h2>X-Salada Duplo</h2>
                <small class="descricao">Dois hambúrguers artesanais,
                    maionese, catchup,
                    mostarda, milho, ervilha,
                    tomate, cebola, queijo
                    mussarela, alface e ovo frito.</small>

                    <div class="boxPreco">
                <span class="preco">R$ 32,99</span>
            </div>
            </div>
        </div>


@endsection
