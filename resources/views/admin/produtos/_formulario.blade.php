@csrf

@if ($errors->any())

<ul class="alert alert-danger p-2 list-unstyled">
    @foreach ($errors->all() as $erro)
        <li>{{$erro}}</li>
    @endforeach
</ul>

@endif

<div class="col-md-12">
    <label for="titulo"
           class="form-label">Título</label>

    <input type="text"
           class="form-control"
           name="titulo"
           id="titulo"
           placeholder="Insira o Título">

</div>

<div class="col-md-6">
    <label for="categoria"
           class="form-label">Categoria</label>
    <select name="categoria_id"
            class="form-control">
        <option>Selecione a Categoria</option>

        @foreach ($categorias as $categoria)

            <option value="{{ $categoria->id }}"> {{ $categoria->titulo }} </option>

        @endforeach



    </select>

</div>

<div class="col-md-12">
    <label for="descricao"
           class="form-label">Descrição</label>
    <textarea class="form-control"
              name="descricao"
              id="descricao"
              rows="10"
              ></textarea>
</div>

<div class="col-md-3">
    <label for="valor"
           class="form-label">Valor</label>
    <input type="text"
           class="form-control"
           name="valor"
           id="valor">
</div>


<div class="col-md-12">
    {{-- @if ($produtos->foto)

        <img src="{{ $produtos->foto }}" alt="" width="80">

    @endif --}}
    <label for="foto"
           class="form-label">Foto</label>
    <input type="file"
           class="form-control"
           name="foto"
           id="foto">

</div>




<div class="col-12">
    <button type="submit"
            class="btn btn-primary">Salvar</button>
</div>
