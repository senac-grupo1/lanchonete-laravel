<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lanchonete Burguer</title>
    <meta name="keyworks" content="Lanchonete, hamburguer, porção, pizza">
    <meta name="description" content="Melhor Lanchonete de Marília, Lanchonete Burguer">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=PT+Serif&family=Poppins:wght@300;400;600&display=swap"
        rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=PT+Serif&family=Poppins:wght@300;400;600&family=Shantell+Sans:wght@400;600;700&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/fonts/style.css">

</head>

<body>

    <header id="cabecalho">
        <div id="barraTopo">
            <div id="btnMenu" >
                <i class="icon-menu"></i>
            </div>
            <div id="logoTipo">
                <h1><img src="/img/logoTipo.png" alt="Lanchonete Burguer" height="40px"></h1>
            </div>
            <nav id="menuPrincipal">

                <button id="btnClose">&#10006;</button>
                <div class="">
                    <img src="/img/logoTipo.png" alt="Lanchonete Burguer" height="40px">
                </div>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Lanchonete</a></li>
                    <li><a href="#">Loacalização</a></li>
                    <li><a href="#">Contato</a></li>
                </ul>
            </nav>

            <div id="usuario">
                <img src="/img/Foto_Perfil.jpg" alt="Perfil" height="40px" class="formatoRedondo">
            </div>
        </div>
        <div id="overlay"></div>
        <div id="pesquisar">
            <form action="" method="get">
                <input type="search" name="produto" placeholder="Oque gostaria de come?">
                <button type="submit">
                    <i class="icon-busca"></i>
                </button>
            </form>
        </div>


    </header>

    <main id="conteudo">

        @yield('conteudo')

    </main>

    <footer id="rodape">
        <nav id="menuRodape">
            <ul>
                <li>
                    <i class="icon-home"></i>
                </li>

                <li>
                    <i class="icon-cesta"></i>
                </li>

                <li>
                    <i class="icon-favorito"></i>
                </li>

                <li>
                    <i class="icon-pedido"></i>
                </li>

                <li>
                    <i class="icon-usuario"></i>
                </li>
            </ul>
        </nav>
    </footer>

    <script src="/js/main.js"></script>
</body>

</html>
