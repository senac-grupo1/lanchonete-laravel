<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categorias')->insert([
            [
                'titulo' => 'Pizza',
                'icone' => 'storege/icones/icones.svg'
            ],
            [
                'titulo' => 'Bebida',
                'icone' => 'storege/icones/icones.svg'
            ],
            [
                'titulo' => 'Lanche',
                'icone' => 'storege/icones/icones.svg'
            ],
            [
                'titulo' => 'Suco',
                'icone' => 'storege/icones/icones.svg'
            ],
            [
                'titulo' => 'Porção',
                'icone' => 'storege/icones/icones.svg'
            ],
        ]);
    }
}
