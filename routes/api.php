<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ProdutoController;
use App\Http\Controllers\Api\UsuarioController;
use App\Http\Controllers\Api\CategoriaController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);


Route::middleware('auth:sanctum')->group(function(){
    Route::get('/usuario', [UsuarioContoller::class, 'usuario']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::get('/categorias', [CategoriaController::class, 'listarCategorias']);
Route::get('/produtos', [ProdutoController::class, 'listarProdutos']);
Route::get('/produto/{id}', [ProdutoController::class, 'mostrarProduto']);
Route::post('/cadastrar', [UsuarioController::class, 'cadastrar']);


